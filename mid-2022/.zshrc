if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# List available theme for zsh
# apple, robbyrussel, af-magic, afowler, amuse, avit, awesomepanda,
# blinks, bureau
#ZSH_THEME="arrow"
#Top 12 Oh My Zsh Themes for Productive Developers
#1. Eastwood
#2. Simple
#3. Lukerandall
#4. Gozilla
#5. Kphoen
#6. Jonathan
#7. Minimal
#8. Apple
#9. Gnzh
#10. Nanotech
#11. Agnoster
#12. Miloshadzic
#ZSH_THEME="gnzh"
ZSH_THEME="agnoster"

plugins=(
  git
  bundler
  dotenv
  rake
  #rbenv
  #ruby
  #autoenv
  autojump
  kubectl
  ubuntu
  vim-interaction
  postgres
  vscode
  pip
  kubectx
  golang
  history
  git
  gcloud
  fzf
  aws
  docker
  docker-compose
)
# env variables
source $ZSH/oh-my-zsh.sh
alias composer="php /usr/local/bin/composer.phar"
alias kube="kubectl"
export PATH="/usr/local/opt/mysql-client/bin:$PATH"
export PATH="/usr/local/flutter/bin:$PATH"
export CONNECTION_STRING=postgresql://admin:3ngineeringHungryHub2020!@175.41.162.97:5439/hh_dwh
export drive="/media/heisenberg/hardrive"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '${HOME}/google-cloud-sdk/path.zsh.inc' ]; then . '${HOME}/heisenberg/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '${HOME}/google-cloud-sdk/completion.zsh.inc' ]; then . '${HOME}/google-cloud-sdk/completion.zsh.inc'; fi

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"


autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/terraform terraform
#source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
